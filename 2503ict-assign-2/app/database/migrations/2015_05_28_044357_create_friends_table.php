<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFriendsTable extends Migration {

	/*
		Up function: Creates the required fields for the Friends table (pivot)
	*/
	public function up()
	{
		Schema::create('friends', function($table)
		{
			$table->increments('id');
			$table->timestamps();
			
			// Foreign keys
			$table->integer('user_id');
			$table->integer('friend_id');
		});
	}

	public function down()
	{
		Schema::dropIfExists('friends');
	}

}
