<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {

	/*
		Up function: Creates the required fields for the Post table
	*/
	public function up()
	{
		Schema::create('posts', function($table)
		{
			$table->increments('id');
			$table->string('title');
			$table->text('message');
			$table->enum('privacy', ['Public', 'Friends', 'Private']);
			$table->timestamps();
			
			// Foreign keys
			$table->string('user_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('posts');
	}

}
