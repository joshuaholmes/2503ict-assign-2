<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration {

	/*
		Up function: Creates the required fields for the Comment table
	*/
	public function up()
	{
		Schema::create('comments', function($table)
		{
			$table->increments('id');
			$table->text('message');
			$table->timestamps();
			
			// Foreign keys
			$table->integer('post_id');
			$table->integer('user_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('comments');
	}

}
