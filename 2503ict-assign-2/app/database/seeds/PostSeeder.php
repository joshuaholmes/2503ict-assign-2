	<?php
	
	class PostSeeder extends Seeder {
	
	    public function run()
	    {
	        DB::table('posts')->delete();
	
	        $post = new Post;
     		$post->title = 'Android M';
     		$post->message = 'Google announces new Android M at Google I/O!';
     		$post->privacy = 'Public';
     		$post->user_id = 2;
     		$post->save();
     		
     		$post = new Post;
     		$post->title = 'Apple';
     		$post->message = 'Apple products are revolutionary';
     		$post->privacy = 'Public';
     		$post->user_id = 1;
     		$post->save();
     		
     		$post = new Post;
     		$post->title = 'Who cares?';
     		$post->message = 'Im bringing space to the consumer and all they argue about are phones.';
     		$post->privacy = 'Private';
     		$post->user_id = 4;
     		$post->save();
     		
     		$post = new Post;
     		$post->title = 'The Apple Watch';
     		$post->message = 'Watch sales will give me a nice little bonus.';
     		$post->privacy = 'Friends';
     		$post->user_id = 3;
     		$post->save();
     		
     		$post = new Post;
     		$post->title = 'Gooooogle';
     		$post->message = 'Very excited for what else ATAP will create.';
     		$post->privacy = 'Public';
     		$post->user_id = 5;
     		$post->save();
     		
     		$post = new Post;
     		$post->title = 'Hi guys';
     		$post->message = 'PiedPiper is ready to stream any video. Guys?';
     		$post->privacy = 'Public';
     		$post->user_id = 10;
     		$post->save();
     		
     		$post = new Post;
     		$post->title = 'Windows 10';
     		$post->message = 'Our newest operating system, Windows 10 is due for realease July 29!!!';
     		$post->privacy = 'Public';
     		$post->user_id = 7;
     		$post->save();
     		
     		$post = new Post;
     		$post->title = 'I miss the good old days';
     		$post->message = 'Back when it was easier and the innovation came faster.';
     		$post->privacy = 'Public';
     		$post->user_id = 8;
     		$post->save();
     		
     		$post = new Post;
     		$post->title = 'Google I/O';
     		$post->message = 'Was daunting to stand up there.';
     		$post->privacy = 'Friends';
     		$post->user_id = 9;
     		$post->save();
     		
     		$post = new Post;
     		$post->title = 'New job';
     		$post->message = 'Ive been promoted to CDO!';
     		$post->privacy = 'Public';
     		$post->user_id = 6;
     		$post->save();
	    }
	
	}