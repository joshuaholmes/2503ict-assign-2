	<?php
	
	class CommentSeeder extends Seeder {
	
	    public function run()
	    {
	        DB::table('comments')->delete();
	
	        $comment = new Comment;
     		$comment->message = 'We know.';
     		$comment->post_id = 2;
     		$comment->user_id = 2;
     		$comment->save();
     		
     		$comment = new Comment;
     		$comment->message = 'Heard it all before Steve.';
     		$comment->post_id = 2;
     		$comment->user_id = 5;
     		$comment->save();
     		
     		$comment = new Comment;
     		$comment->message = 'If its true it can be said as many times as it needs to.';
     		$comment->post_id = 2;
     		$comment->user_id = 3;
     		$comment->save();
     		
     		$comment = new Comment;
     		$comment->message = 'Im going to stay clear of this one.';
     		$comment->post_id = 2;
     		$comment->user_id = 4;
     		$comment->save();
     		
     		$comment = new Comment;
     		$comment->message = 'Honestly, must you?';
     		$comment->post_id = 2;
     		$comment->user_id = 2;
     		$comment->save();
     		
     		$comment = new Comment;
     		$comment->message = 'What?';
     		$comment->post_id = 2;
     		$comment->user_id = 4;
     		$comment->save();
     		
     		$comment = new Comment;
     		$comment->message = 'Not you, Tim.';
     		$comment->post_id = 2;
     		$comment->user_id = 2;
     		$comment->save();
     		
     		$comment = new Comment;
     		$comment->message = 'Cant hear you over the sound of my bank account.';
     		$comment->post_id = 2;
     		$comment->user_id = 3;
     		$comment->save();
     		
     		$comment = new Comment;
     		$comment->message = 'Is that necessary? Seriously.';
     		$comment->post_id = 2;
     		$comment->user_id = 5;
     		$comment->save();
     		
     		$comment = new Comment;
     		$comment->message = 'Play nice.';
     		$comment->post_id = 2;
     		$comment->user_id = 1;
     		$comment->save();
     		
     		$comment = new Comment;
     		$comment->message = 'They really are beautiful.';
     		$comment->post_id = 2;
     		$comment->user_id = 6;
     		$comment->save();
     		
     		$comment = new Comment;
     		$comment->message = 'Really should hire more people. What a brilliant job.';
     		$comment->post_id = 5;
     		$comment->user_id = 2;
     		$comment->save();
     		
     		$comment = new Comment;
     		$comment->message = 'Now, thats what Im talking about!';
     		$comment->post_id = 5;
     		$comment->user_id = 4;
     		$comment->save();
     		
     		$comment = new Comment;
     		$comment->message = 'Thanks boss!';
     		$comment->post_id = 1;
     		$comment->user_id = 9;
     		$comment->save();
     		
     		$comment = new Comment;
     		$comment->message = 'Back when you stole my ideas?';
     		$comment->post_id = 8;
     		$comment->user_id = 1;
     		$comment->save();
     		
     		$comment = new Comment;
     		$comment->message = 'Please...';
     		$comment->post_id = 8;
     		$comment->user_id = 7;
     		$comment->save();
     		
     		$comment = new Comment;
     		$comment->message = 'Congrats.';
     		$comment->post_id = 10;
     		$comment->user_id = 1;
     		$comment->save();
     		
     		$comment = new Comment;
     		$comment->message = 'Well deserved.';
     		$comment->post_id = 10;
     		$comment->user_id = 3;
     		$comment->save();
	    }
	
	}