	<?php
	
	class UserSeeder extends Seeder {

        public function run()
        {
            DB::table('users')->delete();
    
            $user = new User;
     		$user->email = 'steve@apple.com';
     		$user->name = 'Steve Jobs';
     		$user->password = Hash::make('apple');
     		$user->dob = '1955-02-24';
     		$user->image = 'http://www.deyoungmedia.com/wp-content/uploads/2014/07/steve-jobs-morreu-brasil-153927.jpg';
     		$user->save();
     		
     		$user = new User;
     		$user->email = 'sergey@google.com';
     		$user->name = 'Sergey Brin';
     		$user->password = Hash::make('google');
     		$user->dob = '1973-08-21';
     		$user->image = 'http://hdwallpapersfit.com/wp-content/uploads/2015/02/smile-sergey-brin-wallpapers.jpg';
     		$user->save();
     		
     		$user = new User;
     		$user->email = 'tim@apple.com';
     		$user->name = 'Tim Cook';
     		$user->password = Hash::make('apple');
     		$user->dob = '1960-11-01';
     		$user->image = 'http://img1.nymag.com/imgs/daily/intelligencer/2014/10/30/30-tim-cook.w750.h560.2x.jpg';
     		$user->save();
     		
     		$user = new User;
     		$user->email = 'richard@virgin.com';
     		$user->name = 'Richard Branson';
     		$user->password = Hash::make('virgin');
     		$user->dob = '1950-07-18';
     		$user->image = 'http://www.lifeport.co.uk/wp-content/uploads/2013/04/richardbranson.jpg';
     		$user->save();
     		
     		$user = new User;
     		$user->email = 'larry@google.com';
     		$user->name = 'Larry Page';
     		$user->password = Hash::make('google');
     		$user->dob = '1973-03-26';
     		$user->image = 'http://www.wired.com/images_blogs/business/2012/04/larrypage.jpg';
     		$user->save();
            
            $user = new User;
     		$user->email = 'jony@apple.com';
     		$user->name = 'Jony Ive';
     		$user->password = Hash::make('apple');
     		$user->dob = '1967-02-27';
     		$user->image = 'http://cdn.cultofmac.com/wp-content/uploads/2013/11/jonyivecars1.jpg';
     		$user->save();
     		
     		$user = new User;
     		$user->email = 'satya@microsoft.com';
     		$user->name = 'Satya Nadella';
     		$user->password = Hash::make('microsoft');
     		$user->dob = '1967-08-19';
     		$user->image = 'http://static.dnaindia.com/sites/default/files/2014/02/21/satya-nadella.jpg';
     		$user->save();
     		
     		$user = new User;
     		$user->email = 'bill@microsoft.com';
     		$user->name = 'Bill Gates';
     		$user->password = Hash::make('microsoft');
     		$user->dob = '1955-10-28';
     		$user->image = 'http://images.dailytech.com/nimage/bill-gates-desk-picture.jpg';
     		$user->save();
     		
     		$user = new User;
     		$user->email = 'sundar@google.com';
     		$user->name = 'Sundar Pichai';
     		$user->password = Hash::make('google');
     		$user->dob = '1972-01-01';
     		$user->image = 'https://pbs.twimg.com/profile_images/481231649128980480/9hpv14pc.jpeg';
     		$user->save();
     		
     		$user = new User;
     		$user->email = 'richard@piedpiper.com';
     		$user->name = 'Richard Hendricks';
     		$user->password = Hash::make('piedpiper');
     		$user->dob = '1982-03-10';
     		$user->image = 'http://images.tvrage.com/news/hbo-comedy-pilot-silicon-valley-casts-nine-roles.jpg';
     		$user->save();
        }
    }