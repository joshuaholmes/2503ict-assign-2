	<?php
	
	class FriendSeeder extends Seeder {
	
	    public function run()
	    {
	        DB::table('friends')->delete();
	
	        $friend = new Friend;
     		$friend->user_id = 1;
     		$friend->friend_id = 4;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 4;
     		$friend->friend_id = 1;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 2;
     		$friend->friend_id = 4;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 4;
     		$friend->friend_id = 2;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 3;
     		$friend->friend_id = 4;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 4;
     		$friend->friend_id = 3;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 5;
     		$friend->friend_id = 4;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 4;
     		$friend->friend_id = 5;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 1;
     		$friend->friend_id = 3;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 3;
     		$friend->friend_id = 1;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 2;
     		$friend->friend_id = 5;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 5;
     		$friend->friend_id = 2;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 8;
     		$friend->friend_id = 2;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 2;
     		$friend->friend_id = 8;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 4;
     		$friend->friend_id = 8;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 5;
     		$friend->friend_id = 8;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 8;
     		$friend->friend_id = 5;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 7;
     		$friend->friend_id = 8;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 8;
     		$friend->friend_id = 7;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 7;
     		$friend->friend_id = 9;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 9;
     		$friend->friend_id = 7;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 6;
     		$friend->friend_id = 1;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 1;
     		$friend->friend_id = 6;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 3;
     		$friend->friend_id = 6;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 6;
     		$friend->friend_id = 3;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 4;
     		$friend->friend_id = 6;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 6;
     		$friend->friend_id = 4;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 4;
     		$friend->friend_id = 7;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 7;
     		$friend->friend_id = 4;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 4;
     		$friend->friend_id = 8;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 8;
     		$friend->friend_id = 4;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 4;
     		$friend->friend_id = 9;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 9;
     		$friend->friend_id = 4;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 2;
     		$friend->friend_id = 9;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 9;
     		$friend->friend_id = 2;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 5;
     		$friend->friend_id = 9;
     		$friend->save();
     		
     		$friend = new Friend;
     		$friend->user_id = 9;
     		$friend->friend_id = 5;
     		$friend->save();
	    }
	
	}