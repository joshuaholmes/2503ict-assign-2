
<?php

class PostController extends BaseController {

    /*
        Get post function: called to retrieve a single post. Loads with pagination if appropriate (comments exceed 8).
    */
    public function getPost($id)
    {
        $user = Auth::user();
        $post = Post::find($id);
        $post->load('comments');
        $comments = Comment::where('post_id', '=', $id)->paginate(8);
        
        return View::make('post.index')->withPost($post)->withUser($user)->withComments($comments);
    }
    
    /*
        Edit post function: called to edit a post and display the edit post page. Includes validation to ensure correct user has called it.
    */
    public function editPost($id)
    {
        $user = Auth::user();
        $post = Post::find($id);
        
        // CHECK IF IT IS THE LOGGED IN USER
        if ($post->user_id != $user->id)
            die("You don't own that post");
        
        return View::make('post.edit')->withPost($post)->withUser($user);
    }
    
    /*
        Edit post action: called to save an editied post. Returns to the single post page.
    */
    public function editPostAction()
    {
        $id = Input::get('id');
        
        $post = Post::find($id);
        $post->title = Input::get('title');
        $post->message = Input::get('message');
        $post->privacy = Input::get('privacy');
        $post->save();
        
        return Redirect::to(url("post/$id"));
    }
    
    /*
        Add post action: called to add a new post to the site. Includes validation of inputs as per Post model rules. Returns to home page.
    */
    public function addPostAction()
    {
        $validator = Validator::make(Input::all(), Post::$rules);
        
        if ($validator->fails())
            return Redirect::back()->withInput()->withErrors($validator->messages());
    
        $post = new Post;
        $post->title = Input::get('title');
        $post->message = Input::get('message');
        $post->privacy = Input::get('privacy');
        $post->user_id = Auth::user()->id;
        $post->save();
        
        Session::flash('success', 'Post created successfully');
        return Redirect::to(url("/"));
    }
    
    /*
        Delete post action: called to delete a post. Validates that the authenticated user has called it. Returns to the home page.
    */
    public function deletePostAction($id)
    {
        $post = Post::find($id);
        
        // CHECK IF IT IS THE LOGGED IN USER
        if ($post->user_id != Auth::user()->id)
            die("You don't own that post");
        
        $comments = Comment::where('post_id', '=', $post->id);
        
        $comments->delete();
        
        $post->delete();
        
        Session::flash('success', 'Post removed successfully');
        return Redirect::to(url("/"));
    }
}