<?php

class HomeController extends BaseController {

	/*
        Home function: called to load home page with appropriate posts as per authentication.
    */
	public function home()
	{
		$posts = Post::with('user')->with('comments')->orderBy('created_at', 'desc')->get();
		$user = Auth::user();
		
		if ($user) $user->load('friends');

		return View::make('home')->withPosts($posts)->withUser($user);
	}
	
	/*
        Documentation function: called to load documentation page.
    */
	public function documentation()
	{
		return View::make('documentation.index');
	}

}
