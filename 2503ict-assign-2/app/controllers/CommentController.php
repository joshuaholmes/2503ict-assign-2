
<?php

class CommentController extends BaseController {

    /*
        Add comment function: called to add a new comment to a post. Error validator to ensure smooth failure and redirect to post page once done.
    */
    public function addCommentAction()
    {
        $validator = Validator::make(Input::all(), Comment::$rules);
        
        if ($validator->fails())
            return Redirect::back()->withInput()->withErrors($validator->messages())->with('error_id', Input::get('post_id')) ;
            
        $post_id = Input::get('post_id');
        
        $comment = new Comment;
        $comment->user_id = Auth::user()->id;
        $comment->post_id = $post_id;
        $comment->message = Input::get('comment');
        $comment->save();
        
        Session::flash('success', 'Comment added successfully');
        return Redirect::to(url("post/$post_id"));
    }
    
    /*
        Delete comment function: called to delete a comment from a post. Error validator to ensure smooth failure and that the user is correct. Redirects to post page when done.
    */
    public function deleteCommentAction($id)
    {
        $comment = Comment::find($id);
        
        if (!$comment) die("Error deleting comment");
        
        // CHECK IF IT IS THE LOGGED IN USER
        if ($comment->user_id != Auth::user()->id)
            die("You don't own this comment");
        
        $post_id = $comment->post_id;
        $comment->delete();
        
        Session::flash('success', 'Comment removed successfully');
        return Redirect::to(url("post/$post_id"));
    }
}