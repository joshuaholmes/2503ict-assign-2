<?php

class UserController extends BaseController {
    
    /*
        Create function: called to display the create user page.
    */
    public function create()
	{
		return View::make('user.create');
	}
	
	/*
        Show function: called to display a user's profile. Paginates friends feed. Displays content appropriate to the authenticated user.
    */
	public function show($id)
	{
		$user = Auth::user();
		$profile = User::find($id);
		$friends = $profile->friends()->paginate(5);
		
		$is_friend = $user ? $user->friendsWith($profile) : 0;
		
		if ($profile) $profile->load(['posts' => function ($query){
			$query->latest();
		}, 'friends']);
		
		return View::make('user.index')->withProfile($profile)->withUser($user)->withIsFriend($is_friend)->withFriends($friends);
	}
	
	/*
        Add friend function: called to add a new friend. Returns to friends profile.
    */
	public function addFriend()
	{
		$other = User::find(Input::get('friend_id'));
		$other->addFriend(Auth::user());
		
		return Redirect::to(url("/user/$other->id"));
	}
	
	/*
        Remove friend action: called to remove a friend. Returns to the removed friends profile.
    */
	public function removeFriend()
	{
		$other = User::find(Input::get('friend_id'));
		$other->removeFriend(Auth::user());
		
		return Redirect::to(url("/user/$other->id"));
	}
	
	/*
        View friend function: called to view a list of all friends appropriate to the profile viewed from OR the authenticated user.
    */
	public function viewFriends($id)
	{
		$profile = User::find($id);
		
		if (!$profile) die("User not found");
		
		$profile->load('friends');
		
		return View::make('user/friends')->withProfile($profile);
	}
	
	/*
        Login function: called to login and auth a user. Authenticates input data. Returns to previous page.
    */
	public function login()
	{
		$userdata = array(
		'email' => Input::get('username'),
		'password' => Input::get('password')
		);
		
		if (Auth::attempt($userdata))
		{
			return Redirect::to(URL::previous());
		}
		else
		{
			Session::put('login_error', 'Uh, oh. Login failed. Try again.');
			return Redirect::to(URL::previous())->withInput();
		}
	}
	
	/*
        Logout function: called to logout a user. Returns to home page.
    */
	public function logout()
	{
		Auth::logout();
		Session::flush();
		return Redirect::action('HomeController@home');
	}
    
    /*
        Store function: called tostore a new user's data. Authenticates data. Returns to new users profile, logged in.
    */
    public function store()
	{
		$input = Input::all();
		$v = Validator::make($input, User::$rules);
		
		if ($v->passes())
		{
		$password = $input['password'];
		$encrypted = Hash::make($password);
		
		$user = new User;
		$user->email = $input['email'];
		$user->password = $encrypted;
		$user->dob = $input['dob'];
		$user->name = $input['name'];
		$user->image = $input['image'];
		$user->save();
		
		Auth::attempt(array('email' => $input['email'], 'password' => $input['password']));
		
		return Redirect::to(url("/user/$user->id"));
		}
		else
		 {
		 // Show validation errors
		 return Redirect::back()->withInput()->withErrors($v);
		 }
	}
	
	/*
        Search action: called to search for user's. Returns to search page with results or errors.
    */
	public function searchAction() {
		$search = Input::get('search');

  		$results = User::where('name', 'LIKE', "%$search%")->get();
		$user = Auth::user();
		return View::make('search.results')->withResults($results)->withUser($user)->withSearch($search);
	}
	
	/*
        Edit user function: called to take user's to the edit page.
    */
	public function editUser($id)
    {
        $user = Auth::user();
        $profile = User::find($id);
        
        // CHECK IF IT IS THE LOGGED IN USER
        if ($profile->id != $user->id)
            die("You don't own that profile");
        
        return View::make('user.edit')->withProfile($profile)->withUser($user);
    }
	
	/*
        Update function: called to update a user's details. Authenticates data so that if it isn't changed it won't error from the User model rules. Returns to users profile.
    */
	public function update($id)
	{
		$input = Input::all();
		$v = Validator::make($input, array('name' => 'required', 'dob' => 'required'));
		
		 if ($v->passes())
		 {
		 	$password = $input['password'];
			$user = User::find($id);
			
			if ($password != "")
			{
				$encrypted = Hash::make($password);
				$user->password = $encrypted;
			}
			
			if ($input['email'] != $user->email)
			{
				$email_validator = Validator::make($input, array('email' => 'required|email|unique:users'));
				
				if ($email_validator->fails())
					return Redirect::back()->withInput()->withErrors($email_validator->messages());
					
				$user->email = $input['email'];
			}
			
			if ($input['image'])
			{
				$image_validator = Validator::make($input, array('image' => 'image') );
				
				if ($image_validator->fails())
					return Redirect::back()->withInput()->withErrors( $image_validator->messages() );
					
				$user->image = $input['image'];
			}
			
			 $user->name = $input['name'];
			 $user->dob = $input['dob'];
			 $user->save();
			 return Redirect::to(url("/user/$user->id"));
		 }
		 else
		 {
		 // Show validation errors
		 return Redirect::back()->withErrors($v);
		 }
	}

}
