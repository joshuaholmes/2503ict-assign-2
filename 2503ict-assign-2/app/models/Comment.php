<?php

class Comment extends Eloquent {

	protected $table = 'comments';
    
    /*
		Rules for the data entered for comments
		
		The other functions below also link the comment to a post and user.
	*/
    public static $rules = array(
	    'comment' => 'required'
	    );
    
    function post()
    {
        return $this->belongsTo('Post');
    }
    
    function user()
    {
        return $this->belongsTo('User');
    }
}
