<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class User extends Eloquent implements UserInterface, RemindableInterface, StaplerableInterface {

	use UserTrait, RemindableTrait, EloquentTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	
	/*
		Rules for the data entered in the create and edit pages for users
	*/
	public static $rules = array(
    'email' => 'required|email|unique:users',
    'password' => 'required',
    'name' => 'required',
    'dob' => 'required|before:today',
    'image' => 'required|image'
    );
    
    /*
		Constructor for the Stapler components
	*/
    public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('image', [
            'styles' => [
                'large' => '600x600',
                'medium' => '300x300',
                'thumb' => '100x100'
                ]
                ]);
                
                parent::__construct($attributes);
    }
    
    /*
		Age function: Retrieves the age for a user to be used in the profile
	*/
    function age() {
		$birthdate = new DateTime($this->dob);
		$today   = new DateTime('today');
		$age = $birthdate->diff($today)->y;
		return $age;
		
	}
    
    function friends()
    {
        return $this->belongsToMany('User', 'friends', 'user_id', 'friend_id')->withTimestamps();
    }
    
    function posts()
    {
        return $this->hasMany('Post');
    }
    
    /*
		Add friend function: Attaches the users to the friends pivot table
	*/
    public function addFriend(User $user)
    {
        $this->friends()->attach($user->id);
        $user->friends()->attach($this->id);
    }
    
    /*
		Remove friend function: Detaches the users from the friends pivot table
	*/
    public function removeFriend(User $user)
    {
        $this->friends()->detach($user->id);
        $user->friends()->detach($this->id);
    }
    
    /*
		Friends with function: Determines if a user is friends with another so that appropriate posts can be displayed
	*/
    public function friendsWith($other)
    {
        if (!$other) return 0;
        
        $result = Friend::where('user_id', '=', $this->id)->where('friend_id', '=', $other->id)->get();
    
        return !$result->isEmpty();
    }
}
