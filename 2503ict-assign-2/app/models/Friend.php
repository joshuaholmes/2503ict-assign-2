<?php

class Friend extends Eloquent {

	protected $table = 'friends';
    
    protected $hidden = array('id', 'user_id');
    
    function user()
    {
        return $this->belongsToMany('User');
    }
}
