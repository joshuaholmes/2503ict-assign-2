<?php

class Post extends Eloquent {

	protected $table = 'posts';
	
	/*
		Rules function to enable them on each post
		
		Functions below link each post to a user and to the comments
	*/
	public static $rules = array(
	    'title' => 'required',
	    'message' => 'required'
	    );
	    
	function comments()
	{
		return $this->hasMany('Comment');
	}
	
	function user()
	{
		return $this->belongsTo('User');
	}
	
	/*
		Carbon function to change the displayed date/time data for posts
	*/
	public function getCreatedAtAttribute($date)
	{
	
	    return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y H:i');
	}
	
	public function getUpdatedAtAttribute($date)
	{
	    return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y H:i');
	}
}
