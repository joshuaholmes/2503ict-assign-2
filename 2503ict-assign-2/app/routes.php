<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Home Controller bits
Route::get('/', 'HomeController@home');
Route::get('/documentation', 'HomeController@documentation');

// User Controller bits
Route::get('/user/create', 'UserController@create');
Route::get('/user/{id}/friends', 'UserController@viewFriends');
Route::get('search_action', 'UserController@searchAction');
Route::post('user/login', array('as' => 'user.login', 'uses' => 'UserController@login'));
Route::get('user/logout', array('as' => 'user.logout', 'uses' => 'UserController@logout'));
Route::resource('user', 'UserController');

//Post Controller bits
Route::get('post/{id}', 'PostController@getPost');

// Routes inside this group are protected by authentication- you cannot access them if not logged in
Route::group(array('before' => 'auth'), function()
{
    //Show edit page
    Route::get('post/edit/{id}', 'PostController@editPost');
    Route::get('user/edit/{id}', 'UserController@editUser');
    
    
    Route::post('add_friend_action', 'UserController@addFriend');
    Route::post('remove_friend_action', 'UserController@removeFriend');
    Route::post('edit_post_action', 'PostController@editPostAction');
    Route::post('add_comment_action', 'CommentController@addCommentAction');
    Route::post('add_post_action', 'PostController@addPostAction');
    Route::get('delete_post_action/{id}', 'PostController@deletePostAction');
    Route::get('delete_comment_action/{id}', 'CommentController@deleteCommentAction');
});
