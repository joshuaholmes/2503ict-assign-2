
@if ($paginator->getLastPage() > 1)
 
	<ul class="pagination custom">
		<li class="{{ ($paginator->getCurrentPage() == 1) ? 'disabled' : '' }}"><a href="{{ $paginator->getUrl(1) }}"><i class="icon left arrow"></i> Previous</a></li>
    	@for ($i = 1; $i <= $paginator->getLastPage(); $i++)
    		<li class="{{ ($paginator->getCurrentPage() == $i) ? 'active' : '' }}" style="{{ ($paginator->getCurrentPage() == $i) ? 'color:#149c82;' : '' }}"><a href="{{ $paginator->getUrl($i) }}">{{ $i }}</a></li>
    	@endfor
    	<li class="{{ ($paginator->getCurrentPage() == $paginator->getLastPage()) ? ' disabled' : '' }}"><a href="{{ $paginator->getUrl($paginator->getCurrentPage()+1) }}"> Next <i class="icon right arrow"></i></a></li>
	</ul>

@endif