@extends('layouts.master')

@section('title')
    Documentation -- The Social
@stop

@section('content')

    <div class="padding">
        <div class="full col-sm-9">
            <!-- content -->                      
            <div class="row">
                
                 <!-- main col right -->
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h4>Documentation<p><small class="text-muted">Joshua Holmes, s2848433</small></p></h4></div>
                        <div class="panel-body">
                            <div class="clearfix"></div>
                            <p><b>What I Was Able To Complete</b><br>
                            I was able to complete all tasks, including retrieving all posts via a single Eloquent query.<br>
                            <hr>
                            <b>What I Was Not Able To Complete</b><br>
                            Not applicable!<br>
                            <hr>
                            <b>Interesting Approaches</b><br>
                            Again, I wouldn't classify my approach as interesting. Again, I like to try things and see what happens. I had an exisitng project and got underway changing that. I started with adding new pages that would be required. I also created a mock ERD so I knew what database changes would be needed.<br>I then moved onto moving the functions from routes into controllers (and creating new routes accordingly). I then started filling in new functions to create new functionality.<br> It was from here I began testing. As mentioned, I like to make changes and see what happens. I learn more this way and so would break it, and then fix it.<br>I then updated a bunch of UI design and updated all the smaller components, as well as making changes to other bits along the way as needed.<br>Finally I created seeder data and did all the documentation.<br>
                            <hr>
                            <b>Extra Implementation</b><br>
                            I again worked quite heavily on the UI. There is a lot of extra pieces not required as part of this. These include, but is probably not limited to; icons to show what privacy a post has, clickable names on posts and comments, friends display in profile, various session flashes for success messages and profile image seeding. Because it looks better.<br>
                            <hr>
                            <b>References</b><br>
                            My UI is based on a free theme available at Bootstrap Zero: http://www.bootstrapzero.com/bootstrap-template/facebook.
                            <hr>
                            <b>Entity Relationship Diagram</b><br>
                            <img src="/images/erd.png" height="65%" width="65%">
                            <hr>
                            <b>Site Diagram</b><br>
                            <img src="/images/sitedia.png" height="90%" width="90%">
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <!--/row-->
        </div>
        <!-- /col-9 -->
    </div>
    <!-- /padding -->
    
    <!-- Bootstrap JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

@stop