<!-- display results -->
<!DOCTYPE html>
<!-- Results page of associative array search example. -->
<html>
  <head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/style.css">
  </head>

  <body>
    <div class="wrapper">
      <div class="box">
        <!-- main col -->
        <div class="column col-sm-12 col-xs-12" id="main">
                
          <!-- top nav -->
          <div class="navbar navbar-green navbar-static-top">  
            <div class="navbar-header">
              <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle</span>
                <span class="icon-bar"></span>
          			<span class="icon-bar"></span>
          			<span class="icon-bar"></span>
              </button>
              <a href="/" class="navbar-brand logo">The Social</a>
            </div>
              <nav class="collapse navbar-collapse" role="navigation">
                <form class="navbar-form navbar-left" action="/search_action">
                  <div class="input-group input-group-sm" style="max-width:360px;">
                    <input type="text" class="form-control" placeholder="Search" name="search">
                    <div class="input-group-btn">
                      <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                  </div>
                </form>
                <ul class="nav navbar-nav">
                  <li>
                    <a href="/"><i class="glyphicon glyphicon-home"></i> Home</a>
                  </li>
                  @if (Auth::check())
                  <li>
                    <a href="{{ URL::route('user.show', array(Auth::user()->id)) }}" role="button" data-toggle="modal"><i class="glyphicon glyphicon-user"></i> Profile</a>
                  </li>
                  <li>
                    <a href="/user/{{Auth::user()->id}}/friends" role="button" data-toggle="modal"><i class="fa fa-users"></i> Friends</a>
                  </li>
                  @endif
                  <li>
                    <a href="/documentation" role="button" data-toggle="modal"><i class="glyphicon glyphicon-file"></i> Documentation</a>
                  </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-user"></i></a>
                      <ul class="dropdown-menu">
                        @if (Auth::check())
                          <li>{{ link_to_route('user.show', Auth::user()->name, array(Auth::user()->id)) }}</li>
                          <li>{{ link_to_route('user.logout','Sign out') }}</li>
                      	@else
                        	<li><a href="/">Sign in</a></li>
                        	<li>{{ link_to_route('user.create', 'Sign up') }}</li>
                      	@endif
                      </ul>
                    </li>
                  </ul>
              </nav>
          </div>
          <!-- /top nav -->
          @section('content')
          @show
        </div>
      </div>
  </body>
  <!-- Bootstrap JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
</html>

<!-- THEME BASED ON A BOOSTRAP TEMPLATE BY Bootstrap Zero: http://www.bootstrapzero.com/bootstrap-template/facebook -->