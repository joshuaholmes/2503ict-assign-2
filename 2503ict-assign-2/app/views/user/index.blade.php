@extends('layouts.master')

@section('title')
    View Profile -- The Social
@stop

@section('content')
    {{$error_id = Session::get('error_id')}}
    <div class="padding">
        <div class="full col-sm-9">
            <!-- content -->                      
            <div class="row">
                <!-- main col left --> 
                <div class="col-sm-5">
                    <div class="panel panel-default">
                        <div class="panel-heading"><img src="{{ asset($profile->image->url('medium')) }}" class="img-circle pull-right" style="height:100px; width:100px; top:-10px; position:relative;"><h3>{{{ $profile->name }}}</h3></div>
                        <div class="panel-body">
                            <h4>{{{$profile->age()}}} years old</h4><br>
                            <p>{{{$profile->email}}}</p>
                            @if ($user && $user->id == $profile->id)
                                <a class="btn btn-primary pull-right" href="{{{url("user/edit/$user->id")}}}">Edit Profile</a>
                            @endif
                            
                            @if ($user && $user->id != $profile->id && !$is_friend) <!-- If logged in and not friends -->
                                <form method="post" action="/add_friend_action" class="form-inline">
                                    <input type="hidden" name="friend_id" value="{{{ $profile->id }}}">
                                    <button class="btn btn-primary pull-right" type="submit">Add Friend</button>
                                </form>
                            @elseif ($user && $user->id != $profile->id && $is_friend) <!-- If logged in and are friends -->
                                <form method="post" action="/remove_friend_action" class="form-inline">
                                    <input type="hidden" name="friend_id" value="{{{ $profile->id }}}">
                                    <button class="btn btn-danger pull-right" type="submit">Unfriend</button>
                                </form>
                            @endif
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading"><h4>Friends ({{count($profile->friends)}})</h4><a class="btn btn-primary pull-right" style="position:relative; top:-35px;" href="{{{url("/user/$profile->id/friends")}}}">View All Friends</a></div>
                        <div class="panel-body">
                            @if (count($friends) > 0)
                                @foreach ($friends as $friend)
                                <div class="panel-heading">
                                    <img src="{{ asset($friend->image->url('thumb')) }}" class="img-circle pull-left" style="height:65px; width:65px; padding:10px; top:-20px; position:relative;">
                                    <p style="top:20px; position:relative;"><a href="/user/{{$friend->id}}">{{$friend->name}}</a></p><br>
                                </div><br>
                                @endforeach
                            @else
                                <p>{{$profile->name}} has no friends :(<br>Why not add them?</p>
                            @endif
                        </div>
                    </div>
                </div>
                
                @if (count($profile->posts) == 0)              
                <!-- main col right -->
                <div class="col-sm-7">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h4>Nothing to see here, move along...</h4></div>
                    </div>
                </div>
                
                @else
                
                @foreach($profile->posts as $post)
                    @if (($user && $user->id == $post->user_id) || $post->privacy == 'Public' || ($post->privacy == 'Friends' && $user && $user->friendsWith($post->user) ) )
                    <!-- main col right -->
                    <div class="col-sm-7 pull-right">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                    @if ($user && $user->id == $post->user_id)
                                        <a class="btn btn-default btn-xs pull-right" href="{{{ url("delete_post_action/$post->id") }}}">Delete</a>
                                        <a class="btn btn-primary btn-xs pull-right" href="{{{ url("post/edit/$post->id") }}}">Edit</a>
                                    @endif
                                    <img src="{{ asset($profile->image->url('thumb')) }}" class="img-circle pull-left" style="height:65px; width:65px; padding:10px; top:-20px; position:relative;"><h4>{{{$post->title}}}<p><small class="text-muted">
                                        @if ($post->privacy == 'Public')
                                            <p class="glyphicon glyphicon-globe" style="font-size: 12px"></p>
                                        @elseif ($post->privacy == 'Private')
                                            <p class="glyphicon glyphicon-lock" style="font-size: 12px"></p>
                                        @elseif ($post->privacy == 'Friends')
                                            <p class="glyphicon glyphicon-user" style="font-size: 12px"></p>
                                        @endif
                                    <a href="/user/{{$post->user->id}}">{{$post->user->name}}</a> - {{$post->created_at}}</small></p></h4>
                                </div>
                            <div class="panel-body">
                                
                                <p>{{{$post->message}}}</p><br>
                                <div class="clearfix"></div>
                                <hr>
                                    <p><a href="{{{ url("post/$post->id") }}}">{{count($post->comments)}} comments</a></p>
                                    @if ($user)
                                        <form method="post" action="/add_comment_action">
                                            <div class="input-group">
                                                <input type="hidden" name="post_id" value="{{{ $post->id }}}">
                                                @if ($errors->has('comment') && $post->id == $error_id)
                                                    <span class="label label-danger">{{ $errors->first('comment') }}</span>
                                                    <div class="has-error">
                                                        <input type="text" name="comment" class="form-control" placeholder="Add a comment">
                                                    </div>
                                                @else
                                                    <input type="text" name="comment" class="form-control" placeholder="Add a comment">
                                                @endif
                                                <span class="input-group-btn">
                                                <button class="btn btn-default" type="submit">Send</button>
                                                </span>
                                            </div>
                                        </form>
                                    @else
                                        <p><i>Please <a href="/">sign in</a> to comment.</i></p>
                                    @endif
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
                
                @endif
                
            </div>
            <!--/row-->
                          

        </div>
        <!-- /col-9 -->
    </div>
    <!-- /padding -->
    
    <!-- Bootstrap JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

@stop