@extends('layouts.master')

@section('title')
    View Friends -- The Social
@stop

@section('content')

    <div class="padding">
        <div class="full col-sm-9">
            <!-- content -->                      
            <div class="row">
                
                 <!-- main col right -->
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h4>{{$profile->name}}'s Friends</h4></div>
                        <div class="panel-body">
                            <div class="clearfix"></div>
                                @if (count($profile->friends) == 0)
                                    <p>No friends :(</p>
                                @else 
                                    @foreach ($profile->friends as $friend)
                                        <div class="panel-heading">
                                            <img src="{{ asset($friend->image->url('thumb')) }}" class="img-circle pull-left" style="height:65px; width:65px; padding:10px; top:-20px; position:relative;">
                                            <p style="top:20px; position:relative; font-size: 15px;"><a href="/user/{{$friend->id}}">{{$friend->name}}</a></p><br>
                                        </div><br>
                                    @endforeach
                                @endif
                        </div>
                    </div>
                </div>
            </div>
            <!--/row-->
                          

        </div>
        <!-- /col-9 -->
    </div>
    <!-- /padding -->
    
    <!-- Bootstrap JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

@stop