@extends('layouts.master')

@section('title')
    Edit Account -- The Social
@stop

@section('content')

    <div class="padding">
        <div class="full col-sm-9">
            <!-- content -->                      
            <div class="row">
                
                 <!-- main col right -->
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h4>Edit profile</h4></div>
                        <div class="panel-body">
                            <div class="clearfix"></div>
                                <div class="form-group form-horizontal" style="padding:14px;">
                                    {{ Form::model($user, array('method' => 'PUT', 'route' => array('user.update', $user->id), 'files' => true)); }}
                                    <div class="form-group">
                                        @if ($errors->has())
                                            <div class="alert alert-danger">
                                                @foreach ($errors->all() as $error)
                                                    {{ $error }}<br>       
                                                @endforeach
                                            </div>
                                        @endif
                                        {{ Form::label('email', 'Edit your email', array('class' => 'col-sm-2 control-label')) }}
                                        <div class="col-sm-10">
                                            <div class="@if ($errors->has('email')) has-error @endif">
                                                {{ Form::email('email', null, array('class' => 'form-control')) }}
                                            </div>
                                        </div>
                                        @if ($errors->has('email')) <p class="alert alert-danger">{{ $errors->first('email') }}</p> @endif
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('password', 'Change your password', array('class' => 'col-sm-2 control-label')) }}
                                        <div class="col-sm-10">
                                            <div class="@if ($errors->has('password')) has-error @endif">
                                                {{ Form::password('password', array('class' => 'form-control')) }}
                                            </div>
                                        </div>
                                        @if ($errors->has('password')) <p class="alert alert-danger">{{ $errors->first('password') }}</p> @endif
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('name', 'Edit your name', array('class' => 'col-sm-2 control-label')) }}
                                        <div class="col-sm-10">
                                            <div class="@if ($errors->has('name')) has-error @endif">
                                                {{ Form::text('name', null, array('class' => 'form-control')) }}
                                            </div>
                                        </div>
                                        @if ($errors->has('name')) <p class="alert alert-danger">{{ $errors->first('name') }}</p> @endif
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('dob', 'Change your date of birth', array('class' => 'col-sm-2 control-label')) }}
                                        <div class="col-sm-10">
                                            <div class="@if ($errors->has('dob')) has-error @endif">
                                                {{ Form::input('date', 'dob') }}
                                            </div>
                                        </div>
                                        @if ($errors->has('dob')) <p class="alert alert-danger">{{ $errors->first('dob') }}</p> @endif
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('image', 'Change your profile picture', array('class' => 'col-sm-2 control-label')) }}
                                        <div class="col-sm-10">
                                            {{ Form::file('image')}}
                                        </div>
                                    </div>
                                        {{ Form::submit('Update my profile', ['class' => 'btn btn-primary pull-right']) }}
                                        {{ Form::close() }}
                                        <button class="btn btn-default" onclick="history.back(-1)">Cancel</button>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/row-->
        </div>
        <!-- /col-9 -->
    </div>
    <!-- /padding -->
    
    <!-- Bootstrap JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

@stop