@extends('layouts.master')

@section('title')
    View Post -- The Social
@stop

@section('content')

    <div class="padding">
        <div class="full col-sm-9">
            <!-- content -->                      
            <div class="row">
                
                @if (!$post)             
                    <!-- main col right -->
                    <div class="col-md-8 col-md-offset-2">
                        <div class="panel panel-default">
                            <div class="panel-heading"><h4>Whoops, looks like that post isn't there.</h4></div>
                        </div>
                    </div>
                
                @else

                    <!-- main col right -->
                    <div class="col-md-8 col-md-offset-2">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                @if ($user && $user->id == $post->user_id)
                                    <a class="btn btn-default btn-xs pull-right" href="{{{ url("delete_post_action/$post->id") }}}">Delete</a>
                                    <a class="btn btn-primary btn-xs pull-right" href="{{{ url("post/edit/$post->id") }}}">Edit</a>
                                @endif
                                <img src="{{ asset($post->user->image->url('thumb')) }}" class="img-circle pull-left" style="height:65px; width:65px; padding:10px; top:-20px; position:relative;"><h4>{{{$post->title}}}<p><small class="text-muted">
                                @if ($post->privacy == 'Public')
                                    <p class="glyphicon glyphicon-globe" style="font-size: 12px"></p>
                                @elseif ($post->privacy == 'Private')
                                    <p class="glyphicon glyphicon-lock" style="font-size: 12px"></p>
                                @elseif ($post->privacy == 'Friends')
                                    <p class="glyphicon glyphicon-user" style="font-size: 12px"></p>
                                @endif
                                <a href="/user/{{$post->user->id}}">{{$post->user->name}}</a> - {{$post->created_at}}</small></p></h4>
                            </div>
                            <div class="panel-body">
                                <p>{{{$post->message}}}</p><br>
                                <div class="clearfix"></div>
                                <hr>
                                <p>{{{count($comments)}}} comments below</p>
                                @if(Session::has('success'))
                                        <div class="alert alert-success" style="">
                                            <p>{{ Session::get('success') }}</p>
                                        </div>
                                    @endif
                                @if (count($comments) > 0)
                                    @foreach ($comments as $comment)
                                        <div class="panel-heading">
                                            @if ($user && $comment->user->id == $user->id)
                                                <a class="btn btn-default btn-xs pull-right" href="{{{ url("delete_comment_action/$comment->id") }}}">Delete</a>
                                            @endif
                                            <img src="{{ asset($comment->user->image->url('thumb')) }}" class="img-circle pull-left" style="height:65px; width:65px; padding:10px; top:-20px; position:relative;"><p><b><a href="/user/{{$comment->user->id}}">{{$comment->user->name}}</a></b></p>
                                            <p>{{{$comment->message}}}</p>
                                        </div>
                                        <br>
                                    @endforeach
                                    <?php echo $comments->links(); ?>
                                @endif
                                
                                @if ($user)
                        
                                    <form method="post" action="/add_comment_action">
                                        <div class="input-group">
                                            <input type="hidden" name="post_id" value="{{{ $post->id }}}">
                                             @if ($errors->has('comment'))
                                                <span class="label label-danger">{{ $errors->first('comment') }}</span>
                                                <div class="has-error">
                                                    <input type="text" name="comment" class="form-control" placeholder="Add a comment">
                                                </div>
                                            @else
                                                <input type="text" name="comment" class="form-control" placeholder="Add a comment">
                                            @endif
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="submit">Send</button>
                                            </span>
                                        </div>
                                        
                                    </form>
                                @else
                                    <p><i>Please <a href="/">sign in</a> to comment.</i></p>
                                @endif
                            </div>
                        </div>
                    </div>
                
                @endif
                
            </div>
            <!--/row-->
                          
            
                            
        </div>
        <!-- /col-9 -->
    </div>
    <!-- /padding -->
    
    <!-- Bootstrap JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

@stop