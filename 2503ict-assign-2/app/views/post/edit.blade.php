@extends('layouts.master')

@section('title')
    Edit Post -- The Social
@stop

@section('content')

    <div class="padding">
        <div class="full col-sm-9">
            <!-- content -->                      
            <div class="row">
                @if (count($post) == 0)              
                    <!-- main col right -->
                    <div class="col-md-8 col-md-offset-2">
                        <div class="panel panel-default">
                            <div class="panel-heading"><h4>Whoops, looks like that post isn't there.</h4></div>
                        </div>
                    </div>
                @else
    
                    <!-- main col right -->
                    <div class="col-md-8 col-md-offset-2">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <form role="form" method="post" action="{{{ url('edit_post_action')}}}">
                                    <div class="panel-heading"><h4>Edit post "{{{ $post->title }}}"</h4></div>
                                    <div class="form-group" style="padding:14px;">
                                        <input type="hidden" name="id" value="{{{ $post->id }}}"> 
                                        <input type="text" name="title" class="form-control" value="{{{ $post->title }}}"><br>
                                        <textarea name="message" class="form-control">{{{ $post->message }}}</textarea><br>
                                        {{ Form::select('privacy', array('Public' => 'Public', 'Friends' => 'Friends', 'Private' => 'Private'), $post->privacy, array('class' => 'form-control')) }}
                                    </div>
                                    <button class="btn btn-primary pull-right" type="submit">Update</button><ul class="list-inline"></ul>
                                </form>
                                <form action="/">
                                    <button class="btn btn-default" type="submit">Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    
                @endif
                
            </div>
            <!--/row-->
        </div>
        <!-- /col-9 -->
    </div>
    <!-- /padding -->
    
<!-- Bootstrap JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

@stop