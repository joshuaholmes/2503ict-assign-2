@extends('layouts.master')



@section('title')
The Social -- Our network
@stop

@section('content')
    {{ $error_id = Session::get('error_id') }}
    <div class="padding">
        <div class="full col-sm-9">
            <!-- content -->                      
            <div class="row">
                <!-- main col left --> 
                <div class="col-sm-5">
                    <div class="well"> 
                    @if ($user)
                        <form class="form-horizontal" role="form" method="post" action="add_post_action">
                            <h4>What's The Latest?</h4>
                            <div class="form-group" style="padding:14px;">
                                @if ($errors->has('title'))
                                    <span class="label label-danger">{{ $errors->first('title') }}</span>
                                    <div class="has-error">
                                        <input type="text" name="title" class="form-control" placeholder="Title"><br>
                                    </div>
                                @else
                                    <input type="text" name="title" class="form-control" placeholder="Title"><br>
                                @endif
                                
                                @if ($errors->has('message'))
                                    <span class="label label-danger">{{ $errors->first('message') }}</span>
                                    <div class="has-error">
                                        <textarea name="message" class="form-control" placeholder="Message"></textarea><br>
                                    </div>
                                @else
                                    <textarea name="message" class="form-control" placeholder="Message"></textarea><br>
                                @endif
                                
                                <select name="privacy" class="form-control">
                                    <option value="Public">Public</option>
                                    <option value="Friends">Friends</option>
                                    <option value="Private">Private</option>
                                </select>
                            </div>
                            <button class="btn btn-primary pull-right" type="submit">Post</button><br>
                        </form>
                        @else
                            <h4>Login to post or {{ link_to_route('user.create', 'Create an account') }}</h4>
                            <div class="form-group form-horizontal" style="padding:14px;">
                                {{ Form::open(array('url' => secure_url('user/login'))) }}
                                <div class="form-group">
                        	        {{ Form::label('username', 'Username', array('class' => 'col-sm-2 control-label')) }}
                        		    <div class="col-sm-10">
                        		        {{ Form::text('username', null, array('class' => 'form-control')) }}
                        		    </div>
                        		</div>
                        		<div class="form-group">
                            		{{ Form::label('password', 'Password', array('class' => 'col-sm-2 control-label')) }}
                            		<div class="col-sm-10">
                            		    {{ Form::password('password', array('class' => 'form-control')) }}
                        		    </div>
                        		</div>
                        		{{ Form::submit('Login', ['class' => 'btn btn-primary pull-right']) }}
                        		{{Session::get('login_error') }}
                        		{{ Form::close() }}
                            </div>
                        @endif
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="panel-heading"><h4>What's The Social?</h4></div>
                        <div class="panel-body">
                            <p>The Social is a new social network designed for the users by Joshua Holmes. It was developed as part of a project for Web Programming at Griffith Univeristy, Queensland, Australia.<br>This project can be found on <a href="https://bitbucket.org/joshuaholmes/2503ict-assign-2">Bitbucket</a>.<br>More information can be found on the documentation page.</p>
                        </div>
                    </div>
                </div>
                
                @if (count($posts) == 0)              
                <!-- main col right -->
                <div class="col-sm-7">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h4>Nothing to see here, move along...</h4></div>
                    </div>
                </div>
                
                @else
                    @if(Session::has('success'))
                        <div class="col-sm-7 alert alert-success" style="">
                            <p>{{ Session::get('success') }}</p>
                        </div>
                    @endif
                @foreach($posts as $post)
                    
                    @if (($user && $user->id == $post->user_id) || $post->privacy == 'Public' || ($post->privacy == 'Friends' && $user && $user->friendsWith($post->user) ) )
                    <!-- main col right -->
                    <div class="col-sm-7 pull-right">
                        <div class="panel panel-default">
                                <div class="panel-heading">
                                    @if ($user && $user->id == $post->user_id)
                                        <a class="btn btn-default btn-xs pull-right" href="{{{ url("delete_post_action/$post->id") }}}">Delete</a>
                                        <a class="btn btn-primary btn-xs pull-right" href="{{{ url("post/edit/$post->id") }}}">Edit</a>
                                    @endif
                                    <img src="{{ asset($post->user->image->url('thumb')) }}" class="img-circle pull-left" style="height:65px; width:65px; padding:10px; top:-20px; position:relative;"><h4>{{{$post->title}}}<p><small class="text-muted">
                                        @if ($post->privacy == 'Public')
                                        <p class="glyphicon glyphicon-globe" style="font-size: 12px"></p>
                                        @elseif ($post->privacy == 'Private')
                                        <p class="glyphicon glyphicon-lock" style="font-size: 12px"></p>
                                        @elseif ($post->privacy == 'Friends')
                                        <p class="glyphicon glyphicon-user" style="font-size: 12px"></p>
                                        @endif
                                        <a href="/user/{{$post->user->id}}">{{$post->user->name}}</a> - {{$post->created_at}}</small></p></h4>
                                </div>
                            <div class="panel-body">
                                <p>{{{$post->message}}}</p><br>
                                <div class="clearfix"></div>
                                <hr>
                                    <p><a href="{{{ url("post/$post->id") }}}">{{count($post->comments)}} comments</a></p>
                                    @if ($user)
                                    <form method="post" action="/add_comment_action">
                                            <div class="input-group">
                                                <input type="hidden" name="post_id" value="{{{ $post->id }}}">
                                                @if ($errors->has('comment') && $post->id == $error_id)
                                                    <span class="label label-danger">{{ $errors->first('comment') }}</span>
                                                    <div class="has-error">
                                                        <input type="text" name="comment" class="form-control" placeholder="Add a comment">
                                                    </div>
                                                @else
                                                    <input type="text" name="comment" class="form-control" placeholder="Add a comment">
                                                @endif
                                                <span class="input-group-btn">
                                                <button class="btn btn-default" type="submit">Send</button>
                                                </span>
                                            </div>
                                    </form>
                                    @else
                                        <p><i>Please  <a href="/">sign in</a> to comment.</i></p>
                                    @endif
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
                
                @endif
                
            </div>
            <!--/row-->
                          

        </div>
        <!-- /col-9 -->
    </div>
    <!-- /padding -->
    
    <!-- Bootstrap JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

@stop