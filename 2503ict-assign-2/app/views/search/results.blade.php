@extends('layouts.master')

@section('title')
Search Results -- The Social
@stop

@section('content')

    <div class="padding">
        <div class="full col-sm-9">
            <!-- content -->                      
            <div class="row">
                <!-- main col right -->
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h4>Search results for <i>"{{$search}}"</i></h4></div>
                        <div class="panel-body">
                            <div class="clearfix"></div>
                            @if (count($results) == 0)
                                <p>No results found.</p>
                            @elseif ($search == "")
                                <p class="alert alert-danger">You need to enter search text</p>
                            @else
                                @foreach($results as $result)
                                    <div class="panel-heading">
                                        <img src="{{ asset($result->image->url('thumb')) }}" class="img-circle pull-left" style="height:65px; width:65px; padding:10px; top:-20px; position:relative;">
                                        <p style="top:20px; position:relative; font-size: 15px;"><a href="/user/{{$result->id}}">{{$result->name}}</a></p><br>
                                    </div><br>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <!--/row-->
        </div>
        <!-- /col-9 -->
    </div>
    <!-- /padding -->
    
    <!-- Bootstrap JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

@stop